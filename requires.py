from charms.reactive import hook
from charms.reactive import RelationBase
from charms.reactive import scopes
from charmhelpers.core.hookenv import log

class JDBCRequires(RelationBase):
    scope = scopes.GLOBAL

    auto_accessors = ['host', 'url', 'user', 'password', 'driver', 'extended']


    @hook('{requires:jdbc}-relation-{joined,changed}')
    def changed(self):
        self.set_state('{relation_name}.connected')
        if self.connection_string():
            self.set_state('{relation_name}.available')

    @hook('{requires:jdbc}-relation-{departed,broken}')
    def broken(self):
        self.remove_state('{relation_name}.connected')
        self.remove_state('{relation_name}.available')

    def connection_string(self):
        """
        Get the connection string, if available, or None.
        """
        if(self.url() is not None):
            log("CONNECTION STRING "+self.url())
        else:
            log("Connection String empty")
        data = {
            'host': self.host(),
            'url': self.url(),
            'user': self.user(),
            'password': self.password(),
            'driver': self.driver(),
            'extended': self.extended(),
        }
        if self.url():
            return str.format(
                'url={url} user={user} password={password} '
                'driver={driver} extended={extended}',
                **data)
        return None
